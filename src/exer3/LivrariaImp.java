package exer3;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

@WebService(serviceName = "LivrariaOnLine", endpointInterface = "exer3.Livraria")
public class LivrariaImp implements Livraria {

	private static List<Livro> base = new ArrayList<>();

	static {
		base.add(new Livro(1, "Use a cabeca java"));
		base.add(new Livro(2, "Deitel java"));
		base.add(new Livro(3, "Webservice java"));
		base.add(new Livro(4, "JSF em acao"));
	}

	@Override
	public List<Livro> getEstoque() {
		System.out.println("servico de estoque");
		return base;
	}

	@Override
	public Livro getLivro(Integer id) {
		System.out.println("servico de livro");
		for (Livro livro : base) {
			if (livro.getId().equals(id)) {
				return livro;
			}
		}

		return null;
	}

}
