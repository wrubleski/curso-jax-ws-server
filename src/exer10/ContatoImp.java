package exer10;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

@WebService(endpointInterface = "exer10.Telefone")
public class ContatoImp implements Telefone {

	static List<Contato> banco = new ArrayList<>();

	@Override
	public void adicionar(Contato contato) {
		System.out.println("Adicionando no banco");
		banco.add(contato);
	}

	@Override
	public List<Contato> getContatos() {
		System.out.println("Obtendo contatos");
		return banco;
	}

	@Override
	public void deletar(Contato contato) {
		System.out.println("Removendo contato");
		banco.remove(contato);

	}

}
