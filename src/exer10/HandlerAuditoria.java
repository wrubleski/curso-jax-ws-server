package exer10;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.ws.handler.LogicalHandler;
import javax.xml.ws.handler.LogicalMessageContext;
import javax.xml.ws.handler.MessageContext;

public class HandlerAuditoria implements LogicalHandler<LogicalMessageContext> {

	private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");

	@Override
	public boolean handleMessage(LogicalMessageContext context) {
		Boolean saida = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (saida) {
			System.out.println("Auditoria - Saindo do servidor - " + sdf.format(new Date()));

		} else {
			System.out.println("Auditoria - chegando no servidor - " + sdf.format(new Date()));

		}

		return true;

	}

	@Override
	public boolean handleFault(LogicalMessageContext context) {

		return false;
	}

	@Override
	public void close(MessageContext context) {

	}

}
