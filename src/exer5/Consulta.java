package exer5;

public class Consulta {
	private boolean debito;
	private String mensagem;

//	public Consulta() {
//
//	}
//
//	public Consulta(boolean debito, String mensagem) {
//		super();
//		this.debito = debito;
//		this.mensagem = mensagem;
//	}

	public boolean isDebito() {
		return debito;
	}

	public void setDebito(boolean debito) {
		this.debito = debito;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
