package exer5;

import java.math.BigDecimal;

import javax.jws.WebService;

@WebService(serviceName = "VisaEletron", targetNamespace = "http://visa.eletron.consulta", endpointInterface = "exer5.CartaoVisa")
public class CartaoVisaImp implements CartaoVisa {

	private static BigDecimal SALDO = new BigDecimal(10000);

	@Override
	public Consulta debitarSaldo(BigDecimal valor) {
		Consulta consulta = new Consulta();
		if (valor.compareTo(SALDO) <= 0) {
			SALDO = SALDO.subtract(valor);
			consulta.setMensagem("Saldo disponivel - " + SALDO.toString());
			consulta.setDebito(true);
		} else {
			consulta.setMensagem("Saldo insuficiente - ");
		}
		return consulta;
	}

	@Override
	public void creditarSaldo(BigDecimal valor) {
		SALDO = SALDO.add(valor);

	}

	@Override
	public BigDecimal processarTaxa(BigDecimal valor) {
		return null;
	}

}
