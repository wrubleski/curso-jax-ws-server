package exer12;

import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.jws.WebService;

@WebService(endpointInterface = "exer12.ServicoImagem")
public class ServicoImagemImp implements ServicoImagem {

	@Override
	public Image download() {
		try {
			File image = new File("D://imagens-soap//soap.jpg");
			return ImageIO.read(image);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
