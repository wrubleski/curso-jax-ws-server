package exer12;

import java.awt.Image;

import javax.jws.WebService;

@WebService
public interface ServicoImagem {
	Image download();
}
