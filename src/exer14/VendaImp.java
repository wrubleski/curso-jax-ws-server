package exer14;

import java.math.BigDecimal;

import javax.jws.WebService;

@WebService(endpointInterface = "exer14.Venda")
public class VendaImp implements Venda {

	@Override
	public void vender(Produto p) throws VendaException {
		if (p.getNome() == null) {
			throw new VendaException("Nome do produto e obrigatorio.");
		}

		if (p.getNome().trim().equals("")) {
			throw new VendaException("Nome do produto e obrigatorio.");
		}

		if (p.getValor() == null) {
			throw new VendaException("Valor do produto e obrigatorio.");
		}

		if (BigDecimal.ZERO.equals(p.getValor())) {
			throw new VendaException("Valor do produto e obrigatorio.");
		}

		System.out.println("Venda feita com sucesso");
	}

}
