package exer1;

import javax.jws.WebService;

@WebService
public interface Calculadora {
	long somar(int v1, int v2);
}
